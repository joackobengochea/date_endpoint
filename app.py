from datetime import date, datetime
from flask import Flask, jsonify
from flask.logging import create_logger
from flask_restful import Resource, Api, reqparse
from pytz import common_timezones, timezone


app = Flask(__name__)
LOG = create_logger(app)
api = Api(app, prefix='/date')  # prefix for ALB listener rule


class CurrentDate(Resource):
    def post(self):
        try:
            parser = reqparse.RequestParser()
            parser.add_argument('show_time', type=bool, required=True)
            parser.add_argument('timezone', type=str, required=False)
            args = parser.parse_args()
            show_time = args.get('show_time', False)
            tz = args.get('timezone')
            current_date = self.iso_datetime(tz) if show_time else self.iso_date()
            return jsonify({'current_date': current_date})
        except Exception as e:
            LOG.error(e)
            return str(e), 400

    def iso_date(self):
        return date.today().isoformat()

    def iso_datetime(self, tz_string):
        if not tz_string or tz_string not in common_timezones:
            current_date = datetime.now()
        else:
            tz = timezone(tz_string)
            current_date = tz.localize(datetime.now())
        return current_date.isoformat(' ', timespec='seconds')


api.add_resource(CurrentDate, '/current_date')
