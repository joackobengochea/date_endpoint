# Current date endpoint

The endpoint will return date and time in ISO format if `show_time` is true, and the date only otherwise.

## Running the server

To run the server, build and run the docker image with the following commands:
```
docker build -t date_endpoint .
docker run -p 8080:8080 date_endpoint
```

## Endpoint call example

```
curl -X POST localhost:8080/date/current_date --header 'Content-Type: application/json' --data '{"show_time": true}'

```
The response will be something like this:
```
{"current_date":"2020-08-29 22:55:08"}
```

## Timezones

Optionally, the timezone for the current datetime can be added with the field `timezone` in the request data. For example:

```
:~$ curl -X POST localhost:8080/date/current_date --header 'Content-Type: application/json' --data '{"show_time": true, "timezone": "America/Argentina/Buenos_Aires"}'

{"current_date":"2020-08-30 14:44:21-03:00"}

```

By default, `UTC` timezone is used.
