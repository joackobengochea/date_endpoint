FROM python:3.8

WORKDIR /srv/src/

RUN apt-get update && apt-get install -y gcc \
    && apt-get autoremove -y \
    && apt-get clean \
    && rm -rf /tmp/* /var/tmp/* \
    && rm -rf /var/lib/apt/lists/*
ADD requirements.txt /srv/src/
RUN pip install -r requirements.txt
RUN pip freeze
COPY . /srv/src/
EXPOSE 8080
CMD ["uwsgi", "--ini", "uwsgi.ini"]
